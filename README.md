# Gitlab Watchdog
Gitlab watchdog is tiny plugin written for [Bitbar](https://github.com/matryer/bitbar). It is menu bar for Gitlab users to see issues and merge requests assigned to them.

## Gitlab.com status
Overall health of Gitlab.com

![gitlab.com](screenshoot/gitlabstatus.png)

## Overall Menu 

See issues and merge requests assigened to you

![overall](screenshoot/overall.png)

## Pipeline status

Pipeline status of merge requests indicated with color. i.e. red is failing pipeline green is passing pipeline

![pipelines](screenshoot/pipelinestatus.png)

## List notification 

Updated issues an merge requests will be indicated with 🔸

![list](screenshoot/listnotification.png)

## Desktop notification

Whenever issue or merge request is updatded you will get a desktop notification.

![desktop](screenshoot/desktopnotification.png)


# Setup 
- Install [Bitbar](https://github.com/matryer/bitbar)
- Generate [personel access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
- Download `gitlab.5m.rm` and put inside of Bitbar plugin folder
- Open `gitlab.5m.rm` in an text editor and  edit line `ENV.fetch('API_TOKEN', 'your token')` and put your own token. That you generated in second step
- Normally information will be updated every 5 min but if you like more frequent updates you can change file name. i.e. gitlab.3m.rb will update everthing in 3 min 

# Limitations 
- Bitbar doesn't allow reading environment variables at the moment
- Notifications are created using apple script which is old way of doing things. Unfortunately users can't interact with these notifications at the moment. 
