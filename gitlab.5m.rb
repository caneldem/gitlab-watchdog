#!/usr/bin/ruby
# frozen_string_literal: true

# <bitbar.title>Gitlab</bitbar.title>
# <bitbar.version>v1.0</bitbar.version>
# <bitbar.author>Can Eldem</bitbar.author>
# <bitbar.author.github>eldemcan</bitbar.author.github>
# <bitbar.desc>See issues and mr that are assigned to you</bitbar.desc>
# <bitbar.image></bitbar.image>

require 'net/http'
require 'json'
require 'date'
require 'yaml'

class GitlabApp
  BASE_URL = 'https://gitlab.com/api/v4' # change base api if your instance is somehere else
  API_TOKEN = ENV.fetch('API_TOKEN', 'your token') # Bitbar isn't able to read from env variable

  def self.sync_time
    matching_data = /\d{1,2}(m|s|h|d)/.match(__FILE__)
    if matching_data.size.positive?
      frequency = matching_data[0]
      type = matching_data[1]
      @sync_time = GitlabApp.calculate_sync_time(frequency.sub(type, ''), type)
    else
      @sync_time = Time.now.utc
    end
  end

  def self.calculate_sync_time(frequency, type)
    time = Time.now
    frequency = frequency.to_i
    case type
    when 'm'
      time - (frequency * 60)
    when 's'
      time - frequency
    when 'd'
      time - (frequency * 86_400)
    when 'h'
      time - (frequency * 3600)
    else
      Time.now
    end
  end

  GitlabApp.sync_time
  def self.check_gitlab_status
    uri = URI('https://status.gitlab.com/')

    res = Net::HTTP.get_response(uri)

    return 'Gitlab Status Unreachable' unless res.is_a?(Net::HTTPSuccess)

    if /All Systems Operational/.match(res.body)&.size&.positive?
      puts 'Gitlab Status|color=green'
    else
      puts 'Gitlab Status|color=red'
    end
  end

  def self.make_request(url)
    uri = URI(url)
    req = Net::HTTP::Get.new(uri)
    req['PRIVATE-TOKEN'] = API_TOKEN

    res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
      http.request(req)
    end

    return JSON.parse(res&.body) if res.is_a?(Net::HTTPSuccess)

    {}
  end

  def self.get_pipeline_status(item)
    _, project_id, sha = item.fetch_values('title', 'project_id', 'sha')
    data = GitlabApp.make_request("#{BASE_URL}/projects/#{project_id}/pipelines?sha=#{sha}")
    GitlabApp.convert_status_to_color(data&.first&.dig('status'))
  end

  def self.convert_status_to_color(status)
    colors = {
      'running' => 'orange',
      'pending' => 'gray',
      'success' => 'green',
      'failed' => 'red',
      'cancelled' => 'silver',
      'skipped' => 'snow'
    }

    colors.key?(status) ? colors[status] : ''
  end

  def self.check_mr
    data = GitlabApp.make_request("#{BASE_URL}/merge_requests/?state=opened&scope=assigned_to_me&order_by=updated_at")

    data.each do |item|
      color = GitlabApp.get_pipeline_status(item)
      updated = GitlabApp.updated_since?(item['updated_at'])
      notification_symbol = updated == true ? ':small_orange_diamond:' : ''

      GitlabApp.display_notification(item['title'], 'Merge Request Updated') if updated
      puts "--#{item['title']} #{notification_symbol}| color=#{color} href=#{item['web_url']}"
    end
  end

  def self.check_issues
    data = make_request("#{BASE_URL}/issues?state=opened&scope=assigned_to_me&order_by=updated_at")

    data.each do |item|
      updated = GitlabApp.updated_since?(item['updated_at'])
      notification_symbol = updated == true ? ':small_orange_diamond:' : ''
      GitlabApp.display_notification(item['title'], 'Issue Updated') if updated
      puts "--#{item['title']} #{notification_symbol}|href=#{item['web_url']}"
    end
  end

  def self.display_notification(body = '*', title = '*')
    body = body.gsub(/[!@%&'"]/, ' ')
    title = title.gsub(/[!@%&'"]/, ' ')

    command = "osascript -e 'display notification \"#{body}\" with title \"#{title}\"'"
    system(command)
  end

  def self.updated_since?(time)
    timea = DateTime.iso8601(time)&.to_time
    timea >= @sync_time
  end
end

GitlabApp.check_gitlab_status
puts '---'
puts 'issues'
GitlabApp.check_issues
puts 'merge requests'
GitlabApp.check_mr
